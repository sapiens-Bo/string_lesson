﻿#include <iostream>
#include <string>

int main()
{
	std::string string1{ "String say Hello World" };
	size_t stringLength{ string1.length() };
	std::cout << string1 << " " << stringLength << std::endl;
	std::cout << string1.front() << " " << string1.back() << std::endl;
	return 0;
}
